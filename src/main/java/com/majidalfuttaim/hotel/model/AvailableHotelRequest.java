package com.majidalfuttaim.hotel.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AvailableHotelRequest {

	@NotNull
	private Date fromDate;
	@NotNull
	private Date toDate;
	@NotBlank
	@Size(max = 3, min = 2)
	private String city;
	@NotNull
	private Integer numberOfAdults;
}
