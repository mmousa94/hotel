package com.majidalfuttaim.hotel.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Mahmood Mousa
 * @since 3-mar-2020
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Response {

	private Boolean success;
	private Integer code;
	private Date time;
	private Long size;
	private Object data;
	private String message;

}
