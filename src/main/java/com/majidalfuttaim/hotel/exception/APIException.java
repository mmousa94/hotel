package com.majidalfuttaim.hotel.exception;

import com.majidalfuttaim.hotel.controller.base.Handler;

/**
 * @author Mahmood Mousa
 * @since 3-mar-2020
 *
 *        Super Class for all application Exceptions To Extends handled in {@link Handler}
 */
public class APIException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public APIException(String message) {
		super(message);
	}

}
