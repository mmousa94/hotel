package com.majidalfuttaim.hotel.exception;

/**
 * 
 * @author Mahmood Mousa
 * @since 3-mar-2020
 *
 */
public class ValidationException extends APIException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ValidationException(String message) {
		super(message);
	}

}
