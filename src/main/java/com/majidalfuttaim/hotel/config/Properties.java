package com.majidalfuttaim.hotel.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Mahmood Mousa
 * @since 3-mar-2020
 **/
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("config")
@Setter
@Getter
public class Properties {

	private List<String> hotelProviderNames;

}
