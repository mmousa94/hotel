package com.majidalfuttaim.hotel.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.majidalfuttaim.hotel.controller.base.BaseController;
import com.majidalfuttaim.hotel.model.AvailableHotelRequest;
import com.majidalfuttaim.hotel.model.Response;
import com.majidalfuttaim.hotel.service.IHotelService;

/**
 * 
 * @author Mahmood
 * @since 3-mar-2020
 * 
 *        ** Model Class to represent hotel entity
 */
@RestController
@RequestMapping("/hotel")
public class HotelController extends BaseController {

	@Autowired
	private IHotelService hotelService;

	@PostMapping("available")
	public ResponseEntity<Response> getUserLayouts(@RequestBody @Valid AvailableHotelRequest request) {
		return successResposne(hotelService.listAvailableHotels(request.getFromDate(), request.getToDate(),
				request.getCity(), request.getNumberOfAdults()));
	}
}
