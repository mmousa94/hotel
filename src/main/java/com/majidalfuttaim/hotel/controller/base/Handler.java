package com.majidalfuttaim.hotel.controller.base;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.majidalfuttaim.hotel.exception.ValidationException;
import com.majidalfuttaim.hotel.model.Response;

/**
 * @author Mahmood Mousa
 * @since 3-mar-2020
 *
 *
 *        Handler controller to handle all thrown exceptions
 * 
 *
 **/
@ControllerAdvice
public class Handler extends BaseController {

	@ExceptionHandler(ValidationException.class)
	public ResponseEntity<Response> handleValidationException(ValidationException e, HttpServletRequest request) {
		return errorResposne(e.getMessage());
	}

	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ResponseEntity<Response> handleParamMissingException(MissingServletRequestParameterException e) {
		return errorResposne("missing param " + e.getParameterName());
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<Response> handleParameterException(MethodArgumentTypeMismatchException e,
			HttpServletRequest request) {
		return errorResposne("incorrect value for the url parameters");
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<Response> handleMessageNotReadable(HttpMessageNotReadableException e,
			HttpServletRequest request) {
		return errorResposne("Request not Readable check the encoding , parameters  and body if any exists");
	}

	@ExceptionHandler(HttpMediaTypeNotSupportedException.class)
	public ResponseEntity<Response> handleMediaTypeException(HttpMediaTypeNotSupportedException e,
			HttpServletRequest request) {
		return errorResposne(" Un supported media type please make sure the request has application/json content type");
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ResponseEntity<Response> handleRescourceNotFound(HttpRequestMethodNotSupportedException e,
			HttpServletRequest request) {
		return errorResposne(
				" unsupported HTTP method for the desired end point change the request method and try again");
	}

	@ExceptionHandler(NoHandlerFoundException.class)
	public ResponseEntity<Response> noHandlerFoundException(NoHandlerFoundException e, HttpServletRequest request) {
		return errorResposne(
				"the desired controller has been reached but no handler found for the end point please check the last part of the URL [../../[THIS PART]]");
	}

	@ExceptionHandler(Throwable.class)
	public ResponseEntity<Response> handleOtherExceptions(Exception e, HttpServletRequest request) {
		return fatalResponse("Internal Server Error");

	}

}
