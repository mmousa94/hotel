package com.majidalfuttaim.hotel.controller.base;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.majidalfuttaim.hotel.config.Properties;
import com.majidalfuttaim.hotel.factory.IResposneFactory;
import com.majidalfuttaim.hotel.model.Response;



/**
 * @author Mahmood Mousa
 * @since 3-mar-2020
 * 
 *        Base Controller for all controllers to extends
 *
 **/
public class BaseController {

	@Autowired
	protected Properties properties;

	@Autowired
	private IResposneFactory resposneFactory;

	protected ResponseEntity<Response> successResposne(Object data) {
		return resposneFactory.get(HttpStatus.OK, data);
	}

	protected ResponseEntity<Response> errorResposne(String message) {
		return resposneFactory.get(HttpStatus.BAD_REQUEST, message);
	}

	protected ResponseEntity<Response> fatalResponse(String message) {
		return resposneFactory.get(HttpStatus.INTERNAL_SERVER_ERROR, message);
	}

	protected ResponseEntity<Response> response(HttpStatus status, Object data) {
		return resposneFactory.get(status, data);
	}

}
