package com.majidalfuttaim.hotel.controller.base;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mahmood Mousa
 * @since 3-mar-2020
 * 
 * 
 *        Simple Ping controller
 *
 */
@RestController
@RequestMapping("ping")
public class PingController {

	@RequestMapping(method = RequestMethod.GET)
	public String ping() {
		return "Hello world !";
	}

}
