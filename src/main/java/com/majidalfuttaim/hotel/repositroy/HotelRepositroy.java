package com.majidalfuttaim.hotel.repositroy;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.majidalfuttaim.hotel.config.Properties;
import com.majidalfuttaim.hotel.domain.Hotel;
import com.majidalfuttaim.hotel.domain.Room;

/**
 * @author Mahmood Mousa
 * @since 3-mar-2020
 * 
 *        Repository class to simulate DB Operations
 * 
 **/
@Component
public class HotelRepositroy {

	private Set<Hotel> hotels;
	@Autowired
	private Properties properties;

	@PostConstruct
	public void init() {
		hotels = new HashSet<>();
		Calendar calendar = Calendar.getInstance();

		// hotel one
		Hotel hotel1 = new Hotel();
		hotel1.setFare(75.5);
		hotel1.setName("hotel 1");
		hotel1.setProvider(properties.getHotelProviderNames().get(0));
		Room room1 = new Room();
		room1.setName("room1");
		List<Date> dates1 = new ArrayList<Date>();
		dates1.add(new Date());
		dates1.add(new Date());
		dates1.add(new Date());
		room1.setReservationDates(dates1);

		Room room2 = new Room();
		room2.setName("room2");

		List<Date> dates2 = new ArrayList<Date>();
		dates2.add(new Date());
		dates2.add(new Date());
		dates2.add(new Date());

		room2.setReservationDates(dates2);
		// hotel two

		Hotel hotel2 = new Hotel();
		hotel2.setFare(75.5);
		hotel2.setName("hotel 1");
		hotel2.setProvider(properties.getHotelProviderNames().get(1));
		Room room3 = new Room();
		room3.setName("room3");
		List<Date> dates3 = new ArrayList<Date>();
		dates3.add(new Date());
		dates3.add(new Date());
		dates3.add(new Date());
		room3.setReservationDates(dates1);

		Room room4 = new Room();
		room4.setName("room4");

		List<Date> dates4 = new ArrayList<Date>();
		dates4.add(new Date());
		dates4.add(new Date());
		dates4.add(new Date());

		room4.setReservationDates(dates4);

	}

	public List<Hotel> findAll() {
		List<Hotel> entities = new ArrayList<Hotel>(hotels);
		return entities;
	}

	public void add(Hotel entity) {
		hotels.add(entity);
	}

}
