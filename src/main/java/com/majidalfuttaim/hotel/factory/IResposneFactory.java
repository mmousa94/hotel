package com.majidalfuttaim.hotel.factory;



import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.majidalfuttaim.hotel.model.Response;


/**
 * @author Mahmood Mousa
 * @since 3-mar-2020
 * 
 *        Factory for Responses returned from API
 **/
public interface IResposneFactory {

	/**
	 * 
	 * @param status as HttpStatus
	 * @param data   as Object in case of success response the data will be included
	 *               in response payload in case of fail response will be included
	 *               in the response message
	 * @return response as ResponseEntity of type Response filled with data in
	 *         {@link Response}
	 */
	ResponseEntity<Response> get(HttpStatus status, Object data);

}
