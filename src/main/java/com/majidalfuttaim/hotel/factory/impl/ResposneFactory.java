package com.majidalfuttaim.hotel.factory.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.majidalfuttaim.hotel.factory.IResposneFactory;
import com.majidalfuttaim.hotel.model.Response;

/**
 * @author Mahmood Mousa
 * @since 3-mar-2020
 * 
 *        Implementation for {@link IResposneFactory}
 **/

public class ResposneFactory implements IResposneFactory {

	@Override
	public ResponseEntity<Response> get(HttpStatus status, Object data) {
		Response resposne = null;

		data = Objects.isNull(data) ? new ArrayList<>() : data;

		if (status.is2xxSuccessful()) {
			Long dataSize = (data instanceof Collection<?>) ? ((Collection<?>) data).size() : 1L;
			resposne = new Response(true, status.value(), new Date(), dataSize, data, "Success");
		} else if (status.is4xxClientError()) {
			resposne = new Response(false, status.value(), new Date(), 0L, new ArrayList<>(), data.toString());
		} else if (status.is5xxServerError()) {
			resposne = new Response(false, status.value(), new Date(), 0L, new ArrayList<>(), data.toString());
		}

		return new ResponseEntity<Response>(resposne, status);
	}

}
