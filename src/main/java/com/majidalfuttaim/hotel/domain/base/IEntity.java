package com.majidalfuttaim.hotel.domain.base;

import java.io.Serializable;

/**
 * Interface for all JPA entities to implement.
 * 
 * @author Mahmood Mousa
 * @since 3-mar-2020
 *
 */
public interface IEntity {

	/**
	 * All entities must have an ID field.
	 */
	Serializable getId();

}