package com.majidalfuttaim.hotel.domain.base;

import java.io.Serializable;

/**
 * 
 * @author Mahmood Mousa
 * @since 3-mar-2020
 * 
 *        Base implementation for {@link IEntity} 
 */
public class BaseEntity implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -227356014560794055L;

	protected Long id;

	@Override
	public Serializable getId() {
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BaseEntity other = (BaseEntity) obj;
		if (getId() == null) {
			if (other.getId() != null) {
				return false;
			}
		} else if (!getId().equals(other.getId())) {
			return false;
		}
		return true;
	}

}
