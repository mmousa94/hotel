package com.majidalfuttaim.hotel.domain;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.util.ObjectUtils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Mahmood
 * @since 3-mar-2020
 * 
 *        ** Model Class to represent hotel entity
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Hotel implements Comparable<Hotel> {

	@NotBlank
	private String name;
	@NotNull
	private Double fare;
	@NotEmpty
	private List<String> amenities;
	@NotBlank
	private String provider;

	private Double rate;

	@NotEmpty
	private List<Room> rooms;

	public Boolean hasAvailableRooms(Date startDate, Date endDate) {
		if (ObjectUtils.isEmpty(rooms)) {
			return false;
		}

		for (Iterator<Room> iterator = rooms.iterator(); iterator.hasNext();) {
			Room value = iterator.next();
			return value.getReservationDates().contains(startDate) || value.getReservationDates().contains(endDate);
		}

		return false;
	}

	@Override
	public int compareTo(Hotel o) {
		if (Objects.isNull(o)) {
			return 0;
		}

		return this.rate < o.getRate() ? 0 : 1;
	}

}
