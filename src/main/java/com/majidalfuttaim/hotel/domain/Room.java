package com.majidalfuttaim.hotel.domain;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.majidalfuttaim.hotel.domain.base.BaseEntity;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Mahmood
 * @since 3-mar-2020
 * 
 *        ** Model Class to represent Room entity
 */
@Setter
@Getter
public class Room extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull
	private String name;

	private List<Date> reservationDates;
}
