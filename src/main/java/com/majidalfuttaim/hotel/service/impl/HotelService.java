package com.majidalfuttaim.hotel.service.impl;

import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.majidalfuttaim.hotel.domain.Hotel;
import com.majidalfuttaim.hotel.repositroy.HotelRepositroy;
import com.majidalfuttaim.hotel.service.IHotelService;

/**
 * 
 * @author Mahmood
 * @since 3-mar-2020
 * 
 *        ** Implementation for {@link IHotelService}
 */
@Service
public class HotelService implements IHotelService {

	@Autowired
	private HotelRepositroy repositroy;

	@Override
	public List<Hotel> listAvailableHotels(Date fromDate, Date toDate, String city, Integer numberOfAdults) {
		List<Hotel> hotels = repositroy.findAll();

		for (Iterator<Hotel> iterator = hotels.iterator(); iterator.hasNext();) {
			Hotel value = iterator.next();
			if (!value.hasAvailableRooms(fromDate, toDate)) {
				iterator.remove();
			}
		}
		Collections.sort(hotels);
		
		return hotels;
	}

}
