package com.majidalfuttaim.hotel.service;

import java.util.Date;
import java.util.List;

import com.majidalfuttaim.hotel.domain.Hotel;

/**
 * 
 * @author Mahmood
 * @since 3-mar-2020
 * 
 *        ** Hotel Service to handle operations for Hotels
 */
public interface IHotelService {

	/**
	 * 
	 * @param fromDate
	 *            as {@link Date} , represent the start date
	 * @param toDate
	 *            as {@link Date} , represent the end date
	 * @param city
	 *            as {@link String} the city code
	 * @param numberOfAdults
	 *            as {@link Integer} the number of Adults
	 * @return the available Hotels depends on start and end date in a specific city
	 *         as {@link List} of {@link Hotel}
	 */
	public List<Hotel> listAvailableHotels(Date fromDate, Date toDate, String city, Integer numberOfAdults);

	
	
	

}
